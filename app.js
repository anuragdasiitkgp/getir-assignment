// Require libraries and imports
import { getRecords } from './controllers/record.controller.js';
import mongoose from 'mongoose';
import express from 'express';
import dotenv from 'dotenv';
import bodyParser from 'body-parser';

// Initializations
dotenv.config();
const app = express();
(async() => {
  try {
    await mongoose.connect(process.env.MONGODB_URL);
    console.log("Successfully connected to database");
  } catch(e) {
    console.log("DB connection failed");
    console.error(e);
  }
})();

// Middlewares
app.use(bodyParser.json());

// Routers
app.post('/', getRecords);

export default app;