import RecordModel from '../models/record.model.js';

/**
 * A generic function for aggregation based queries
 * @param {Array} pipelines 
 * @returns 
 */
export async function findRecordsByAggregation(pipelines) {
  let result = await RecordModel.aggregate(pipelines);
  return result;
}

/**
 * A more specific function for our use-case, it will build the query pipeline
 * and call findRecordsByAggregation to get the result
 * @param {Number} minCount 
 * @param {Number} maxCount 
 * @param {Date} start 
 * @param {Date} end 
 * @returns 
 */
export async function filterByDateAndTotalCount(minCount, maxCount, start, end) {
  let aggregationPipelines = [
    {
      $project: {
        totalCount: {
          $sum: "$counts"
        },
        key: 1,
        createdAt: 1
      }
    },
    {
      $match: {
        totalCount: {
          $gte: minCount,
          $lte: maxCount
        },
        createdAt: {
          $gte: new Date(start),
          $lte: new Date(end)
        }
      }
    }
  ];
  let result = await findRecordsByAggregation(aggregationPipelines);
  return result;
}