import mongoose from 'mongoose';
import constants from '../constants.js';

// Record Schema
const RecordSchema = new mongoose.Schema({
  key: String,
  value: String,
  createdAt: Date,
  counts: mongoose.Schema.Types.Array
});

// Initialize model
const RecordModel = mongoose.model(constants.RECORD_MODEL, RecordSchema, constants.RECORD_COLLECTION);

// Export record model
export default RecordModel;