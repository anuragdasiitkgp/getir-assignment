import { filterByDateAndTotalCount } from '../services/record.service.js';
import constants from '../constants.js';

/**
 * The controller for our POST route, which'll handle the record queries by count and date
 * @param {Object} req 
 * @param {Object} res 
 */
export async function getRecords(req, res) {
  let { body } = req;
  body = body || {};

  // Log request
  console.log(body);

  let { startDate, endDate, minCount, maxCount } = body;

  // Render an error message if any of the params are missing
  if(!startDate || !endDate || !minCount || !maxCount) {
    return res.status(400).json({code: constants.ERR_CODE_MISSING_FIELD, msg: constants.ERR_MSG_MISSING_FIELD});
  }

  // Count validation
  if(minCount > maxCount) {
    return res.status(400).json({code: constants.ERR_CODE_INVALID_COUNT, msg: constants.ERR_MSG_INVALID_COUNT});
  }

  // Date validation
  startDate = new Date(startDate);
  endDate = new Date(endDate);
  if(startDate > endDate) {
    return res.status(400).json({code: constants.ERR_CODE_INVALID_DATE, msg: constants.ERR_MSG_INVALID_DATE});
  }

  // Invoke service to fetch query results
  try {
    let records = await filterByDateAndTotalCount(minCount, maxCount, startDate, endDate);

    // Log response
    console.log(records);

    return res.status(200).json({code: constants.SUCCESS_CODE, msg: constants.SUCCESS_MSG, records});
  } catch(e) {
    console.error(e);
    return res.status(400).json({code: constants.ERR_CODE_UNEXPECTED, msg: constants.ERR_MSG_UNEXPECTED});
  }
}