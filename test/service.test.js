import chai, { expect } from 'chai';
import chaiHttp from 'chai-http';
import { filterByDateAndTotalCount } from '../services/record.service.js';
chai.use(chaiHttp);

// Constants
let url = `http://localhost:${process.env.PORT || 8080}`;

// Test values
let correctParams = { 
  "startDate": "2016-12-26", 
  "endDate": "2016-12-28", 
  "minCount": 2700, 
  "maxCount": 3000 
};

/**
 * Testing service
 */
describe('Route service should', async() => {
  it('return values according to filters', async() => {
    let res = await filterByDateAndTotalCount(correctParams.minCount, correctParams.maxCount, correctParams.startDate, correctParams.endDate);
    expect(res).to.be.an('array');
    for(let i in res) {
      expect(res[i]).to.contain.property('key');
      expect(res[i]).to.contain.property('createdAt');
      expect(res[i]).to.contain.property('totalCount');
      expect(res[i].totalCount).to.be.at.least(correctParams.minCount);
      expect(res[i].totalCount).to.be.at.most(correctParams.maxCount);
      expect(res[i].createdAt).to.be.at.least(new Date(correctParams.startDate));
      expect(res[i].createdAt).to.be.at.most(new Date(correctParams.endDate));
    }
  });
});