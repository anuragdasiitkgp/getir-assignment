import mongoose from 'mongoose';
import app from '../app.js';
import constants from '../constants.js';
import chai, { expect } from 'chai';
import chaiHttp from 'chai-http';
chai.use(chaiHttp);

// Constants
let url = `http://localhost:${process.env.PORT || 8080}`;

// Test values
let correctParams = { 
  "startDate": "2016-01-26", 
  "endDate": "2018-02-02", 
  "minCount": 2700, 
  "maxCount": 3000 
};

let startMissing = { 
  "endDate": "2018-02-02", 
  "minCount": 2700, 
  "maxCount": 3000 
};

let endMissing = { 
  "startDate": "2016-01-26", 
  "minCount": 2700, 
  "maxCount": 3000 
};

let minCountMissing = { 
  "startDate": "2016-01-26", 
  "endDate": "2018-02-02",
  "maxCount": 3000 
};

let maxCountMissing = { 
  "startDate": "2016-01-26", 
  "endDate": "2018-02-02", 
  "minCount": 2700
};

let incorrectDate = { 
  "startDate": "2018-01-26", 
  "endDate": "2016-02-02", 
  "minCount": 2700, 
  "maxCount": 3000 
};

let incorrectCount = { 
  "startDate": "2016-01-26", 
  "endDate": "2018-02-02", 
  "minCount": 3700, 
  "maxCount": 3000 
};

// Hooks
before(async() => {
  await mongoose.connect(process.env.MONGODB_URL);
});

after(async() => {
  mongoose.connection.close();
});


/**
 * Testing controller e2e
 */
describe('POST / route should', async() => {

  it('return an error if any of the param is missing', async() => {
    let requests = [startMissing, endMissing, minCountMissing, maxCountMissing];
    for(let i in requests) {
      let res = await chai.request(app).post('/').send(requests[i]);
      const {body} = res;
      expect(res).to.have.status(400);
      expect(body).to.contain.property('msg');
      expect(body).to.contain.property('code');
      expect(body).to.not.contain.property('records');
      expect(body.code).to.equal(constants.ERR_CODE_MISSING_FIELD);
      expect(body.msg).to.equal(constants.ERR_MSG_MISSING_FIELD);
    }
  });

  it('return an error if start date is greater than end date', async() => {
    let res = await chai.request(app).post('/').send(incorrectDate);
    const {body} = res;
    expect(res).to.have.status(400);
    expect(body).to.contain.property('msg');
    expect(body).to.contain.property('code');
    expect(body).to.not.contain.property('records');
    expect(body.code).to.equal(constants.ERR_CODE_INVALID_DATE);
    expect(body.msg).to.equal(constants.ERR_MSG_INVALID_DATE);
  });

  it('return an error if start date is greater than end date', async() => {
    let res = await chai.request(app).post('/').send(incorrectCount);
    const {body} = res;
    expect(res).to.have.status(400);
    expect(body).to.contain.property('msg');
    expect(body).to.contain.property('code');
    expect(body).to.not.contain.property('records');
    expect(body.code).to.equal(constants.ERR_CODE_INVALID_COUNT);
    expect(body.msg).to.equal(constants.ERR_MSG_INVALID_COUNT);
  });

  it('return some records if inputs are correct', async() => {
    let res = await chai.request(app).post('/').send(correctParams);
    const {body} = res;
    expect(res).to.have.status(200);
    expect(body).to.contain.property('msg');
    expect(body).to.contain.property('code');
    expect(body).contain.property('records');
    expect(body.records).to.be.an('array');
    expect(body.code).to.equal(constants.SUCCESS_CODE);
    expect(body.msg).to.equal(constants.SUCCESS_MSG);
  });
});