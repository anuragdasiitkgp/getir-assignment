# Getir Assignment

Getir Nodejs Assignment

## Getting started
To run the app locally:
1. Run `npm i` to install dependencies
2. Setup the environment variables, by copying the contents of .env.sample to .env, in the project root. [Config values are the same, so need not be updated]
3. Run `npm run dev` to run the server