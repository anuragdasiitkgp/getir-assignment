export default {
  RECORD_MODEL: "record",
  RECORD_COLLECTION: "records",
  SUCCESS_MSG: "Success",
  SUCCESS_CODE: 0,
  ERR_MSG_MISSING_FIELD: "Some of the fields are missing from the query payload, please check!",
  ERR_MSG_INVALID_COUNT: "Minimum count should be lesser than or equal to maximum count",
  ERR_MSG_INVALID_DATE: "Start date should be before end date",
  ERR_MSG_UNEXPECTED: "Some unexpected error occured! Please try again",
  ERR_CODE_MISSING_FIELD: 1000,
  ERR_CODE_INVALID_COUNT: 2000,
  ERR_CODE_INVALID_DATE: 3000,
  ERR_CODE_UNEXPECTED: 4000
};