// Imports
import app from './app.js';

// Listen to port
app.listen(process.env.PORT || 80, () => {
  console.log(`Server successfully started at PORT:${process.env.PORT || 80}`);
});